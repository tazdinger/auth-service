import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import * as cookieParser from 'cookie-parser';
import * as fs from 'fs';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { BadRequestException, ValidationError, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  dotenv.config();
  const httpsOptions = {
    key: fs.readFileSync(process.env.SSL_KEY),
    cert: fs.readFileSync(process.env.SSL_CHAIN)
  };
  const app = await NestFactory.create(AppModule, { httpsOptions });
  app.setGlobalPrefix('api');
  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        const errorMessage = validationErrors.map((err) => {
          return { property: err.property, constraints: err.constraints };
        });
        return new BadRequestException(errorMessage);
      }
    })
  );
  app.enableCors({
    credentials: true,
    origin: [/https:\/\/localhost:[0-9]{1,6}$/],
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204
  });
  const config = new DocumentBuilder().setTitle('Jenesei ID').build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/dev', app, document);
  await app.listen(process.env.PORT, () =>
    console.log(`Server started on ${process.env.PORT} port`)
  );
}
bootstrap();
