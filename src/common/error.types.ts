import { HttpStatus } from '@nestjs/common';
import { ApiProperty, PartialType } from '@nestjs/swagger';

export class ErrorResponse {
  @ApiProperty({ description: 'Причина ошибки' })
  message?: string;
  @ApiProperty({ description: 'Тип ошибки' })
  error?: string;
  @ApiProperty({ description: 'Код ошибки', enum: HttpStatus, default: HttpStatus.BAD_REQUEST })
  statusCode?: number;
}

export class ErrorAuthResponse extends PartialType(ErrorResponse) {
  @ApiProperty({ description: 'Причина ошибки', example: 'Не правильный логин или пароль' })
  message?: string;
}

export class ValidationErrorResponse {
  @ApiProperty({
    example: 'password',
    description: 'The property that caused the validation error'
  })
  property: string;

  @ApiProperty({
    example: { minLength: 'password must be longer than or equal to 6 characters' },
    description: 'Validation constraints for the property'
  })
  constraints: Record<string, any>;
}

export class BadRequestResponse {
  @ApiProperty({ example: 'Bad Request', description: 'Error type or message' })
  error: string;

  @ApiProperty({ example: 400, description: 'HTTP status code for the error' })
  statusCode: number;

  @ApiProperty({
    type: [ValidationErrorResponse],
    example: [
      {
        property: 'password',
        constraints: {
          minLength: 'password must be longer than or equal to 6 characters'
        }
      }
    ],
    description: 'Array of validation error details'
  })
  message: ValidationErrorResponse[];
}
