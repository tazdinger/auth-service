import {
  Controller,
  Post,
  Body,
  UseGuards,
  Res,
  Get,
  Req,
  HttpStatus,
  HttpCode
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/signin.dto';
import { SignUpDto } from './dto/signup.dto';
import { LocalAuthGuard } from './guard/local.guard';
import { Request, Response } from 'express';
import JwtAuthGuard from './guard/jwt.guard';
import { RequestWithUser } from './interfaces/requestWithUser.interface';
import RefreshGuard from './guard/refresh.guard';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags
} from '@nestjs/swagger';
import { BadRequestResponse, ErrorAuthResponse, ErrorResponse } from 'src/common/error.types';
import { NotAuthGuard } from './guard/not-auth.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse()
  @ApiResponse({
    status: HttpStatus.MOVED_PERMANENTLY,
    headers: {
      location: {
        schema: {
          type: 'string',
          example: `https://BASE_URL/api/profile/:username`
        }
      }
    }
  })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: ErrorAuthResponse })
  @ApiBody({ type: SignInDto })
  @ApiOperation({ summary: 'Авторизация' })
  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(
    @Body() signInDto: SignInDto,
    @Res({ passthrough: true }) response: Response,
    @Req() request: RequestWithUser
  ) {
    if (request?.cookies?.access) {
      const { username } = request.user;
      response
        .status(HttpStatus.MOVED_PERMANENTLY)
        .redirect(`https://${process.env.BASE_URL}/api/profile/${username}`);
      return;
    }
    const cookie = await this.authService.signIn(signInDto);
    response.setHeader('Set-Cookie', cookie);
    return { result: 'ok' };
  }

  @ApiBody({ type: SignUpDto })
  @ApiCreatedResponse()
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestResponse })
  @ApiResponse({
    status: HttpStatus.MOVED_PERMANENTLY,
    headers: {
      location: { schema: { type: 'string', example: `https://BASE_URL/` } }
    }
  })
  @ApiOperation({ summary: 'Регистрация' })
  @UseGuards(NotAuthGuard)
  @Post('sign-up')
  async signUp(
    @Body() signUpDto: SignUpDto,
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request
  ) {
    if (request?.cookies?.access) {
      response.redirect(`https://${process.env.BASE_URL}/`);
      return;
    }
    const cookie = await this.authService.signUp(signUpDto);
    response.setHeader('Set-Cookie', cookie);
    return { result: 'ok' };
  }

  @ApiOperation({ summary: 'Выход' })
  @ApiOkResponse()
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: ErrorResponse })
  @UseGuards(JwtAuthGuard)
  @Get('sign-out')
  async signOut(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('access');
    response.clearCookie('refresh');
    return { result: 'ok' };
  }

  @ApiOperation({ summary: 'Обновить токены' })
  @ApiOkResponse()
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: ErrorResponse })
  @UseGuards(RefreshGuard)
  @Get('refresh')
  async refresh(@Res({ passthrough: true }) response: Response, @Req() request: RequestWithUser) {
    const { ...user } = request?.user;
    const cookie = await this.authService.refresh(user);
    response.setHeader('Set-Cookie', cookie);
    return { result: 'ok' };
  }
}
