import { CanActivate, ExecutionContext, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class NotAuthGuard implements CanActivate {
  constructor(private readonly jwtService: JwtService) {}
  canActivate(context: ExecutionContext): boolean {
    const request: Request = context.switchToHttp().getRequest();
    const response: Response = context.switchToHttp().getResponse();

    if (!request?.cookies?.access) {
      return true;
    }
    const refreshToken = request.cookies.access;
    try {
      const user: User = this.jwtService.decode(refreshToken);
      response
        .status(HttpStatus.MOVED_PERMANENTLY)
        .redirect(`https://${process.env.BASE_URL}/api/profile/${user.username}`);
      return false;
    } catch (error) {
      return true;
    }
  }
}
