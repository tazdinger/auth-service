import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class SignInDto {
  @ApiProperty({
    description: 'Имя пользователя',
    example: 'IvanIvanov',
    required: true,
    type: String
  })
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({
    description: 'Имя пользователя',
    example: 'password',
    required: true,
    type: String
  })
  @IsNotEmpty()
  @IsString()
  password: string;
}
