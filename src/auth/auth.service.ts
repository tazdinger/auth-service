import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { SignInDto } from './dto/signin.dto';
import { User } from 'src/user/entities/user.entity';
import { SignUpDto } from './dto/signup.dto';
import { UserService } from 'src/user/user.service';
import { TokenPayload } from './interfaces/token.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService
  ) {}
  public async signIn(signInDto: SignInDto): Promise<string[]> {
    const { username, password } = signInDto;
    const user = await this.validateUser(username, password);
    return this.generateJwtCookie(user);
  }

  public async signUp(signUpDto: SignUpDto): Promise<string[]> {
    const { email, username, password } = signUpDto;
    if (await this.userService.findByEmail(email)) {
      throw new BadRequestException('Почта занята другим пользователем');
    }
    if (await this.userService.findByUsername(username)) {
      throw new BadRequestException('Логин занят другим пользователем');
    }
    const hashPassword = await bcrypt.hash(password, 5);
    signUpDto.password = hashPassword;
    const newUser = await this.userService.create(signUpDto);
    return await this.generateJwtCookie(newUser);
  }

  private async generateJwtCookie(user: User): Promise<string[]> {
    try {
      const payload: TokenPayload = {
        _id: user._id,
        email: user.email,
        username: user.username
      };
      const accessToken = await this.jwtService.signAsync(payload);
      const refreshToken = await this.jwtService.signAsync(payload, {
        secret: process.env.REFRESH_SECRET,
        expiresIn: process.env.REFRESH_EXPIRES
      });
      const accessCookie = `access=${accessToken}; HttpOnly; Path=/; Max-Age=${60 * 60 * 24}`;
      const refreshCookie = `refresh=${refreshToken}; HttpOnly; Path=/; Max-Age=${process.env.REFRESH_EXPIRES}`;
      return [accessCookie, refreshCookie];
    } catch (error) {
      throw new Error(`Ошибка генерации cookies: ${error.message}`);
    }
  }
  public async refresh(user: User): Promise<string[]> {
    return await this.generateJwtCookie(user);
  }

  public async validateUser(username: string, password: string): Promise<User> {
    const user = await this.userService.findByUsername(username);
    if (!user) {
      throw new UnauthorizedException('Не правильный логин или пароль');
    }
    if (!(await this.verifyPassword(password, user.password))) {
      throw new UnauthorizedException('Не правильный логин или пароль');
    }
    delete user.password;
    return user;
  }

  private async verifyPassword(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }
}
