import { ObjectId } from 'mongodb';

export interface TokenPayload {
  _id: ObjectId;
  email: string;
  username: string;
}
