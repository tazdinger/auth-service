import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import { TokenPayload } from '../interfaces/token.interface';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          return request?.cookies?.access;
        }
      ]),
      ignoreExpiration: false,
      secretOrKey: process.env.ACCESS_SECRET
    });
  }
  async validate(payload: TokenPayload): Promise<User> {
    return await this.userService.findOne(payload._id);
  }
}
