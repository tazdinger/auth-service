import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserService } from 'src/user/user.service';
import { DbModule } from 'src/db/db.module';
import { userProviders } from 'src/user/user.providers';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { LocalStrategy } from './strategy/local.strategy';
import { RefreshStrategy } from './strategy/refresh.strategy';
import { NotAuthGuard } from './guard/not-auth.guard';

@Module({
  imports: [
    DbModule,
    JwtModule.register({
      global: true,
      secret: process.env.ACCESS_SECRET || 'access',
      signOptions: {
        expiresIn: 60
      }
    })
  ],
  controllers: [AuthController],
  providers: [
    ...userProviders,
    AuthService,
    UserService,
    LocalStrategy,
    JwtStrategy,
    RefreshStrategy,
    NotAuthGuard
  ]
})
export class AuthModule {}
