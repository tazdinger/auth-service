import { Controller, Get, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { ApiExcludeEndpoint, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Profile } from './entities/profile.entity';
import { ErrorResponse } from 'src/common/error.types';
import JwtAuthGuard from 'src/auth/guard/jwt.guard';

@ApiTags('Profile')
@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @ApiResponse({ status: HttpStatus.OK, type: Profile })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: ErrorResponse })
  @Get(':username')
  findOne(@Param('username') username: string) {
    return this.profileService.findOne(username);
  }

  @ApiExcludeEndpoint()
  @UseGuards(JwtAuthGuard)
  @Get('admin/_admin_')
  adminRout() {
    return { admin: true };
  }
}
