import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

export class Profile {
  @ApiProperty({ description: 'Идентификатор', example: '65be89eca0761c4e8b8da439', type: String })
  _id: ObjectId;

  @ApiProperty({ description: 'Почта', example: 'test@test.ru', type: String })
  email: string;

  @ApiProperty({ description: 'Логин', example: 'IvanIvanov', type: String })
  username: string;
}
