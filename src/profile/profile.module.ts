import { Module } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { ProfileController } from './profile.controller';
import { userProviders } from 'src/user/user.providers';
import { DbModule } from 'src/db/db.module';

@Module({
  imports: [DbModule],
  controllers: [ProfileController],
  providers: [...userProviders, ProfileService]
})
export class ProfileModule {}
