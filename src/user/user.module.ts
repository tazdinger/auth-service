import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { userProviders } from './user.providers';
import { DbModule } from 'src/db/db.module';
import { ForbiddenMiddleware } from './middleware/forbidden.middleware';

@Module({
  imports: [DbModule],
  controllers: [UserController],
  providers: [...userProviders, UserService]
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ForbiddenMiddleware).forRoutes('user');
  }
}
