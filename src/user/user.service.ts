import { HttpException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { ObjectId } from 'mongodb';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<User>
  ) {}

  public async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      const user = await this.userRepository.save(createUserDto);
      return user;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async findAll(): Promise<User[]> {
    try {
      const users = await this.userRepository.find();
      if (users.length <= 0) {
        throw new NotFoundException('Пользователи не найдены');
      }

      return users.map((user) => {
        delete user.password;
        return user;
      });
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async findOne(id: string | ObjectId): Promise<User> {
    try {
      const user = await this.userRepository.findOne({ where: { _id: new ObjectId(id) } });
      if (!user) {
        throw new NotFoundException('Пользователь не найден');
      }
      delete user.password;
      return user;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async findByEmail(email: string): Promise<User | null> {
    try {
      return await this.userRepository.findOne({ where: { email } });
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async findByUsername(username: string): Promise<User | null> {
    try {
      return await this.userRepository.findOne({ where: { username } });
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }
}
