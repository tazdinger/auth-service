import { ForbiddenException, NestMiddleware } from '@nestjs/common';

export class ForbiddenMiddleware implements NestMiddleware {
  use() {
    throw new ForbiddenException();
  }
}
