import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ description: 'Почта', example: 'test@test.ru' })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ description: 'Логин', example: 'IvanIvanov', minLength: 6 })
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  username: string;

  @ApiProperty({ description: 'Пароль', example: 'password', minLength: 6 })
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  password: string;
}
