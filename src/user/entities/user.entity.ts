import { Column, Entity, Index, ObjectId, ObjectIdColumn, Unique } from 'typeorm';

@Entity({ name: 'users' })
@Unique(['email'])
@Unique(['username'])
@Index('idx_email', ['email'])
@Index('idx_username', ['username'])
export class User {
  @ObjectIdColumn()
  _id: ObjectId;
  @Column('text')
  username: string;
  @Column('text')
  email: string;
  @Column('text')
  password: string;
}
